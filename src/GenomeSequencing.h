/*
 * GenomeSequencing.h
 *
 *  Created on: Mar 10, 2016
 *      Author: memoks
 */

#ifndef GENOMESEQUENCING_H_
#define GENOMESEQUENCING_H_

#include <vector>
#include <list>
#include <iostream>
#include <sstream>
#include <string>
#include <map>
#include <cstdlib>
#include <cmath>
using namespace std;

class Pair
{
public:
	Pair(int item, list<int>::iterator addr)
	: item(item), addr(addr)
	{
	}

	int item;
	list<int>::iterator addr;
};

class EdgeAdj
{
public:
	int fromId;
	int toId;
	bool traversed;
	string data;

	EdgeAdj(int fromId, int toId, bool traversed = false) :
		fromId(fromId), toId(toId), traversed(traversed)
	{
	}

	virtual ~EdgeAdj() { }
};

class GraphAdj
{
public:
	vector< vector<int> > adjList;
	map<string, int> idToRowIndex;
	vector<string> rowIndexToId;

	GraphAdj() { }
	virtual ~GraphAdj() { }

	void addNode(string node)
	{
		if(idToRowIndex.find(node) == idToRowIndex.end())
		{
			int nodeIndex = (int) adjList.size();
			idToRowIndex[node] = nodeIndex;
			rowIndexToId.push_back(node);

			adjList.push_back(vector<int>());
		}
	}

	void addEdge(string from, string to)
	{
		int fromIndex = idToRowIndex[from];
		int toIndex = idToRowIndex[to];

		addEdge(fromIndex, toIndex);
	}

	void addEdge(int from, int to)
	{
		adjList[from].push_back(to);
	}

	void removeEdge(int from, int to)
	{
		vector<int> fromAdj = adjList[from];
		int toIndex = -1;
		for(int i = 0; i < (int) fromAdj.size(); ++i)
		{
			if(to == fromAdj[i])
			{
				toIndex = i;
				break;
			}
		}

		fromAdj.erase(fromAdj.begin() + toIndex);
	}

	void balance(int* from_out, int* to_out)
	{
		int from = -1;
		int to = -1;

		int* inDegrees = new int[adjList.size()];
		int* outDegrees = new int[adjList.size()];
		for(int i = 0; i < (int) adjList.size(); ++i)
			inDegrees[i] = 0;

		for(int i = 0; i < (int) adjList.size(); ++i)
		{
			vector<int>* vAdj = &adjList[i];
			outDegrees[i] = (int) vAdj->size();

			for(int j = 0; j < (int) vAdj->size(); ++j)
				++inDegrees[(*vAdj)[j]];
		}

		for(int i = 0; i < (int) adjList.size(); ++i)
		{
			if(inDegrees[i] < outDegrees[i])
				to = i;
			else if(inDegrees[i] > outDegrees[i])
				from = i;
		}

		addEdge(from, to);

		delete [] inDegrees;
		delete [] outDegrees;

		*from_out = from;
		*to_out = to;
	}

	string getNodeId(int index)
	{
		return rowIndexToId[index];
	}

	int getNodeCount()
	{
		return (int) adjList.size();
	}

	void printNodes()
	{
		for(map<string, int>::iterator it = idToRowIndex.begin(); it != idToRowIndex.end(); ++it)
		{
			cout << it->first << endl;
		}
	}

	void printEdges()
	{
		for(int i = 0; i < (int) adjList.size(); ++i)
		{
			string from = rowIndexToId[i];
			vector<int> fromAdj = adjList[i];
			for(int j = 0; j < (int) fromAdj.size(); ++j)
			{
				string to = rowIndexToId[fromAdj[j]];
				cout << from << " -> " << to << endl;
			}
		}
	}

	void printAdj()
	{
		for(int i = 0; i < (int) adjList.size(); ++i)
		{
			vector<int> fromAdj = adjList[i];

			if(fromAdj.size() > 0)
			{
				cout << i << " -> " << fromAdj[0];
				for(int j = 1; j < (int) fromAdj.size(); ++j)
				{
					cout << "," << fromAdj[j];
				}
				cout << endl;
			}
			else
			{
				cout << i << endl;
			}
		}
	}

	void printAdjLabel()
	{
		for(int i = 0; i < (int) adjList.size(); ++i)
		{
			string from = rowIndexToId[i];
			vector<int> fromAdj = adjList[i];

			if(fromAdj.size() > 0)
			{
				cout << from << " -> " << rowIndexToId[fromAdj[0]];
				for(int j = 1; j < (int) fromAdj.size(); ++j)
				{
					string to = rowIndexToId[fromAdj[j]];
					cout << "," << to;
				}
				cout << endl;
			}
			else
			{
				cout << from << endl;
			}
		}
	}

	GraphAdj copy()
	{
		GraphAdj cpy;
		for(int i = 0; i < (int) rowIndexToId.size(); ++i)


		for(int i = 0; i < (int) adjList.size(); ++i)
		{
			string from = rowIndexToId[i];
			vector<int> fromAdj = adjList[i];
			cpy.addNode(from);

			for(int j = 0; j < (int) fromAdj.size(); ++j)
			{
				string to = rowIndexToId[fromAdj[j]];
				cpy.addNode(to);
				cpy.addEdge(from, to);
			}
		}

		return cpy;
	}
};

class GenomeSequencing
{
public:
	GenomeSequencing();
	virtual ~GenomeSequencing();

/*
	Solve the String Composition Problem.
     Input: An integer k and a string Text.
     Output: Compositionk(Text) (the k-mers can be provided in any order).
 */
	static vector<string> compositions(string text, int k)
	{
		vector<string> kmers;

		for(int i = 0; i < (int) text.length() - k + 1; ++i)
		{
			string kmer = text.substr(i, k);
			kmers.push_back(kmer);
		}

		return kmers;
	}

/*
	String Spelled by a Genome Path Problem. Reconstruct a string from its genome path.
     Input: A sequence of k-mers Pattern1, … ,Patternn such that the last k - 1 symbols of Patterni are
                equal to the first k-1 symbols of Patterni+1 for 1 ≤ i ≤ n-1.
     Output: A string Text of length k+n-1 such that the i-th k-mer in Text is equal to Patterni  (for 1 ≤ i ≤ n).
 */
	static string stringSpelledByGenomePath(vector<string> genomePath)
	{
		string genome = genomePath[0];
		int charInd = (int) genome.length() - 1;
		for(int i = 1; i < (int) genomePath.size(); ++i)
		{
			string kmer = genomePath[i];
			genome += kmer.at(charInd);
		}

		return genome;
	}

/*
	Solve the Overlap Graph Problem (restated below).
     Input: A collection Patterns of k-mers.
     Output: The overlap graph Overlap(Patterns), in the form of an adjacency list. (You may return the edges in any order.)
 */
	static void overlapGraph(vector<string> patterns)
	{
		GraphAdj g;
		for(int i = 0; i < (int) patterns.size(); ++i)
		{
			g.addNode(patterns[i]);
		}

		int k = (int) patterns[0].length();
		for(int i = 0; i < (int) patterns.size(); ++i)
		{
			string from = patterns[i];
			for(int j = 0; j < (int) patterns.size(); ++j)
			{
				if(i == j)
					continue;

				string to = patterns[j];

				if(from.substr(1, k - 1).compare(to.substr(0, k - 1)) == 0)
					g.addEdge(from, to);
			}
		}

		g.printEdges();
	}

/*
	Solve the De Bruijn Graph from a String Problem.
     Input: An integer k and a string Text.
     Output: DeBruijnk(Text), in the form of an adjacency list.
 */
	static GraphAdj deBrujin(string text, int k)
	{
		vector<string> kmers;
		for(int i = 0; i < (int) text.length() - k + 1; ++i)
			kmers.push_back(text.substr(i, k));

		return GenomeSequencing::deBrujin(kmers);
	}

/*
	DeBruijn Graph from k-mers Problem: Construct the de Bruijn graph from a set of k-mers.
     Input: A collection of k-mers Patterns.
     Output: The adjacency list of the de Bruijn graph DeBruijn(Patterns).
 */
	static GraphAdj deBrujin(vector<string> kmers)
	{
		GraphAdj deBrujin;
		int k = (int) kmers[0].length();

		for(int i = 0; i < (int) kmers.size(); ++i)
		{
			string kmer = kmers[i];
			string from = kmer.substr(0, k - 1);
			string to = kmer.substr(1, k - 1);

			deBrujin.addNode(from);
			deBrujin.addNode(to);
			deBrujin.addEdge(from, to);
		}

		return deBrujin;
	}

/*
 	Solve the Eulerian Cycle Problem.
 	 Input: The adjacency list of an Eulerian directed graph.
 	 Output: An Eulerian cycle in this graph.
 */
	static list<int> eulerianCycle(GraphAdj g)
	{
		return GenomeSequencing::eulerianCycle(g, 0);
	}

/*
	Solve the Eulerian Path Problem.
 	 Input: The adjacency list of a directed graph that has an Eulerian path.
     Output: An Eulerian path in this graph.
 */
	static list<int> eulerianPath(GraphAdj g)
	{
		int begin = 0;
		int end = 0;
		g.balance(&end, &begin);

		list<int> eulerianCycle = GenomeSequencing::eulerianCycle(g, begin);
		eulerianCycle.erase(eulerianCycle.begin());
		list<int> eulerianPath;

		list<int>::iterator balanceEdgePos = eulerianCycle.begin();
		int balancedEdgeIndex = 0;
		int prev  = *balanceEdgePos;
		int curr = 0;
		++balanceEdgePos;

		// Find balance edge
		while(balanceEdgePos != eulerianCycle.end())
		{
			curr = (*balanceEdgePos);
			if(prev == end && curr == begin)
			{
				break;
			}
			prev = curr;
			++balanceEdgePos;
			++balancedEdgeIndex;
		}

		while(balanceEdgePos != eulerianCycle.end())
		{
			eulerianPath.push_back(*balanceEdgePos);
			++balanceEdgePos;
		}

		int i = 0;
		for(list<int>::iterator it = eulerianCycle.begin(); it != eulerianCycle.end(); ++it)
		{
			if(i > balancedEdgeIndex)
				break;

			eulerianPath.push_back(*it);
			++i;
		}

		return eulerianPath;
	}

/*
	Solve the String Reconstruction Problem.
	 Input: An integer k followed by a list of k-mers Patterns.
	 Output: A string Text with k-mer composition equal to Patterns. (If multiple answers exist, you may return any one.)
 */
	static string stringReconstruction(int k, vector<string> kmers)
	{
		GraphAdj g = GenomeSequencing::deBrujin(kmers);

		g.printAdjLabel();

		list<int> eulerianPath = GenomeSequencing::eulerianPath(g);

		list<int>::iterator it = eulerianPath.begin();
		string text = g.getNodeId(*it);
		++it;

		while(it != eulerianPath.end())
		{
			text += g.getNodeId(*it).substr(k - 2);
			++it;
		}

		return text;
	}

/*
	Solve the k-Universal Circular String Problem.
	 Input: An integer k.
	 Output: A k-universal circular string.
 */
	static string kUniversalCircularString(int k)
	{
		int uLimit = (int) pow(2, k);

		vector<string> kmers;
		for(int i = 0; i < k * k; ++i)
		{
			string kmer =  decimalToBinary(i, k);
			kmers.push_back(kmer);
			cout << kmer << endl;
		}

		string text = GenomeSequencing::stringReconstruction(k, kmers);

		return text;
	}

private:
	static string decimalToBinary(int decimal, int k)
	{
		string digit = "";
		string binary = "";
		while(decimal >= 2)
		{
			stringstream stream;
			int leftover = decimal % 2;
			stream << leftover;

			stream >> digit;
			binary = digit + binary;

			decimal /= 2;
		}

		if(decimal != 0)
			binary = "1" + binary;

		while(k >  (int) binary.length())
			binary = "0" + binary;

		return binary;
	}

/*
	Solve the Eulerian Cycle Problem.
	 Input: The adjacency list of an Eulerian directed graph.
	 Output: An Eulerian cycle in this graph.
 */
	static list<int> eulerianCycle(GraphAdj g, int startIndex)
	{
		list<int> eulerianCycle;
		eulerianCycle.push_back(startIndex);
		vector<Pair> exploredNodes;
		exploredNodes.push_back(Pair(startIndex, eulerianCycle.begin()));

		while(!exploredNodes.empty())
		{
			int startNodeIndex = exploredNodes[0].item;
			list<int>::iterator cyclePos = exploredNodes[0].addr;

			vector<int>* startAdj = &g.adjList[startNodeIndex];
			if(startAdj->empty())
			{
				exploredNodes.erase(exploredNodes.begin());
				continue;
			}


			int currentNodeIndex = startNodeIndex;
			vector<int>* currentAdj = &g.adjList[currentNodeIndex];
			++cyclePos;
			do
			{
				int nextNodeIndex = (*currentAdj)[0];
				eulerianCycle.insert(cyclePos, nextNodeIndex);

				--cyclePos;
				exploredNodes.push_back(Pair(nextNodeIndex, cyclePos));
				++cyclePos;
				currentAdj->erase(currentAdj->begin());
				currentAdj = &g.adjList[nextNodeIndex];
				currentNodeIndex = nextNodeIndex;
			}
			while(currentNodeIndex != startNodeIndex);
		}

		return eulerianCycle;
	}

};

#endif /* GENOMESEQUENCING_H_ */
