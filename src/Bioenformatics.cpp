//============================================================================
// Name        : Bioenformatics.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <list>
#include <vector>
#include <iostream>
using namespace std;

#include "GenomeSequencing.h"

int main()
{
// CODE CHALLENGE: Solve the String Composition Problem.
//	vector<string> compositions = GenomeSequencing::compositions("CAATCCAAC", 5);
//	for(int i = 0; i < (int) compositions.size(); ++i)
//		cout << compositions[i] << endl;
//
//	return 0;


// CODE CHALLENGE: Solve the String Spelled by a Genome Path Problem.
//	vector<string> genomePath;
//	string input;
//	getline(cin, input);
//	while(input.compare("") != 0)
//	{
//		genomePath.push_back(input);
//		getline(cin, input);
//	}
//
//	string genome = GenomeSequencing::stringSpelledByGenomePath(genomePath);
//	cout << genome << endl;

// CODE CHALLENGE: Solve the Overlap Graph Problem (restated below).
//	vector<string> kmers;
//	string input;
//	getline(cin, input);
//	while(input.compare("") != 0)
//	{
//		kmers.push_back(input);
//		getline(cin, input);
//	}
//
//	GenomeSequencing::overlapGraph(kmers);

// CODE CHALLENGE: Solve the De Bruijn Graph from a String Problem.
//	int k;
//	string text;
//	cin >> k >> text;
//
//	GenomeSequencing::deBrujin(text, k).printAdj();


//	GenomeSequencing::deBrujin("TAATGCCATGGGATGTT", 2).printAdj();
//	GenomeSequencing::deBrujin("TAATGCCATGGGATGTT", 3).printAdj();
//	GenomeSequencing::deBrujin("TAATGCCATGGGATGTT", 4).printAdj();


// CODE CHALLENGE: Solve the de Bruijn Graph from k-mers Problem.
//	vector<string> kmers;
//	string input;
//	getline(cin, input);
//	while(input.compare("") != 0)
//	{
//		kmers.push_back(input);
//		getline(cin, input);
//	}
//
//	GenomeSequencing::deBrujin(kmers).printAdj();


// CODE CHALLENGE: Solve the Eulerian Cycle Problem.
//	GraphAdj g;
//	string adj;
//	getline(cin, adj);
//	while(adj.compare("") != 0)
//	{
//		int curr = (int) adj.find(" -> ", 0);
//		string from = adj.substr(0, curr);
//		g.addNode(from);
//		curr += 4;
//
//		if((int)adj.find(",") == -1)
//		{
//			string to = adj.substr(curr);
//			g.addNode(to);
//			g.addEdge(from, to);
//		}
//		else
//		{
//			do
//			{
//				int next = (int) adj.find(",", curr);
//				string to = adj.substr(curr, next - curr);
//				g.addNode(to);
//				g.addEdge(from, to);
//				curr = next + 1;
//			}
//			while(curr > 0 && curr < (int) adj.size());
//		}
//
//		getline(cin, adj);
//	}
//
//	list<int> eulerianCycle = GenomeSequencing::eulerianCycle(g);
//	for(list<int>::iterator it = eulerianCycle.begin(); it != eulerianCycle.end(); ++it)
//	{
//		cout << g.getNodeId(*it) << "->";
//	}

// CODE CHALLENGE: Solve the Eulerian Path Problem.
//	GraphAdj g;
//	string adj;
//	getline(cin, adj);
//	while(adj.compare("") != 0)
//	{
//		int curr = (int) adj.find(" -> ", 0);
//		string from = adj.substr(0, curr);
//		g.addNode(from);
//		curr += 4;
//
//		if((int)adj.find(",") == -1)
//		{
//			string to = adj.substr(curr);
//			g.addNode(to);
//			g.addEdge(from, to);
//		}
//		else
//		{
//			do
//			{
//				int next = (int) adj.find(",", curr);
//				string to = adj.substr(curr, next - curr);
//				g.addNode(to);
//				g.addEdge(from, to);
//				curr = next + 1;
//			}
//			while(curr > 0 && curr < (int) adj.size());
//		}
//
//		getline(cin, adj);
//	}
//
//	list<int> eulerianCycle = GenomeSequencing::eulerianPath(g);
//	for(list<int>::iterator it = eulerianCycle.begin(); it != eulerianCycle.end(); ++it)
//	{
//		cout << g.getNodeId(*it) << "->";
//	}

// CODE CHALLENGE: Solve the String Reconstruction Problem.
//	vector<string> kmers;
//	int k;
//	string input;
//	cin >> k;
//	getline(cin, input);
//	getline(cin, input);
//	while(input.compare("") != 0)
//	{
//		kmers.push_back(input);
//		getline(cin, input);
//	}
//
//	cout << GenomeSequencing::stringReconstruction(k, kmers);

// CODE CHALLENGE: Solve the k-Universal Circular String Problem.

	cout << GenomeSequencing::kUniversalCircularString(3) << endl;

	return 0;
}
